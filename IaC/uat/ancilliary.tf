data "aws_vpc" "vpc" {
  tags = {
    Name = var.environment
  }
}

data "aws_region" "current" {}

data "aws_subnets" "private" {
  filter {
    name   = "tag:Name"
    values = ["${var.environment}-backend-${data.aws_region.current.name}a", "${var.environment}-backend-${data.aws_region.current.name}b", "${var.environment}-backend-${data.aws_region.current.name}c"]
  }
}

data "aws_subnets" "public" {
  filter {
    name   = "tag:Name"
    values = ["${var.environment}-public-${data.aws_region.current.name}a", "${var.environment}-public-${data.aws_region.current.name}b", "${var.environment}-public-${data.aws_region.current.name}c"]
  }
}

resource "random_password" "db_root_password" {
  length           = var.min_password_length
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}

resource "random_password" "db_app_password" {
  length           = var.min_password_length
  special          = true
  override_special = "!#$%&*()-_=+[]{}<>:?"
}