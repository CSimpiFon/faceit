output "lb_url" {
  value = module.app.lb_url
}

output "pg_url" {
  value = module.app.pg_url
}