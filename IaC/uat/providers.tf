provider "aws" {
  region = "eu-west-1"
}

terraform {
  required_providers {
    postgresql = {
      source = "cyrilgdn/postgresql"
    }
  }
}

provider "postgresql" {
  host            = module.app.pg_url
  port            = module.app.pg_port
  username        = module.app.pg_root_user
  password        = random_password.db_root_password.result
  connect_timeout = 15
  superuser       = false
}