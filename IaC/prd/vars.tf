variable "environment" {
  description = "Name of the service environment"
  type        = string
}

variable "service" {
  description = "Name of the service environment"
  type        = string
  default     = "faceit"
}

variable "min_password_length" {
  description = "Minimum password length"
  type        = number
  default     = 16
}

variable "APP_VERSION" {
  description = "Application version to deploy"
  type        = string
}