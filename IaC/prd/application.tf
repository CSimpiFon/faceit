module "app" {
  source = "../modules/application"

  environment     = var.environment
  service         = var.service
  vpc_id          = data.aws_vpc.vpc.id
  aws_region      = data.aws_region.current.name
  private_subnets = data.aws_subnets.private.ids
  public_subnets  = data.aws_subnets.public.ids
  docker_image    = "registry.gitlab.com/csimpifon/faceit:${var.APP_VERSION}"
  desired_count   = 1
  min_task        = 1
  max_task        = 2
  task_cpu        = 256 # https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html#fargate-tasks-size
  task_memory     = 512 # https://docs.aws.amazon.com/AmazonECS/latest/developerguide/AWS_Fargate.html#fargate-tasks-size
  backup          = false

  db_instance_class = "db.t4g.micro"

  db_root_password = random_password.db_root_password.result
  db_app_password  = random_password.db_app_password.result
}