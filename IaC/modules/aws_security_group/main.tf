resource "aws_security_group" "security_group" {
  name        = var.name
  description = var.description
  vpc_id      = var.vpc_id

  ingress = var.ingress_rules
  egress  = var.egress_rules

  tags = {
    Name        = var.name
    Service     = var.service
    Environment = upper(var.environment)
  }
}