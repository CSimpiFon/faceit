variable "name" {
  description = "Name of Security Group"
  type        = string
}

variable "description" {
  description = "Description of Security Group"
  type        = string
}

variable "vpc_id" {
  description = "ID of the VPC of the Security Group"
  type        = string
}
variable "service" {
  description = "Service name this Security Group is related to - used as the 'Service' tag"
  type        = string
}

variable "environment" {
  description = "Environment this Security Group is related to - used as the 'Environment' tag"
  type        = string
}

variable "ingress_rules" {
  type = list(object({
    description      = optional(string)
    from_port        = optional(number)
    to_port          = optional(number)
    protocol         = optional(string)
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    prefix_list_ids  = optional(list(string))
    security_groups  = optional(list(string))
    self             = optional(bool)
  }))
}

variable "egress_rules" {
  type = list(object({
    description      = optional(string)
    from_port        = optional(number)
    to_port          = optional(number)
    protocol         = optional(string)
    cidr_blocks      = optional(list(string))
    ipv6_cidr_blocks = optional(list(string))
    prefix_list_ids  = optional(list(string))
    security_groups  = optional(list(string))
    self             = optional(bool)
  }))
}