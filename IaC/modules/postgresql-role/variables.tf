variable "name" {
  description = "The name of the role. Must be unique on the PostgreSQL server instance where it is configured."
  type        = string
}

variable "login" {
  description = "Defines whether role is allowed to log in. Roles without this attribute are useful for managing database privileges, but are not users in the usual sense of the word."
  type        = bool
  default     = false
}

variable "password" {
  description = "Sets the role's password. A password is only of use for roles having the login attribute set to true."
  type        = string
  default     = null
}

variable "superuser" {
  description = "Defines a role's ability to execute CREATE DATABASE."
  type        = bool
  default     = false
}

variable "create_database" {
  description = "Defines a role's ability to execute CREATE DATABASE."
  type        = bool
  default     = false
}

variable "create_role" {
  description = "Defines a role's ability to execute CREATE ROLE. A role with this privilege can also alter and drop other roles."
  type        = bool
  default     = false
}

variable "inherit" {
  description = "Defines whether a role 'inherits' the privileges of roles it is a member of."
  type        = bool
  default     = true
}

variable "replication" {
  description = "Defines whether a role is allowed to initiate streaming replication or put the system in and out of backup mode."
  type        = bool
  default     = false
}

variable "bypass_row_level_security" {
  description = "Defines whether a role bypasses every row-level security (RLS) policy."
  type        = bool
  default     = false
}

variable "connection_limit" {
  description = "If this role can log in, this specifies how many concurrent connections the role can establish."
  type        = number
  default     = -1
}

variable "encrypted_password" {
  description = "Defines whether the password is stored encrypted in the system catalogs."
  type        = bool
  default     = true
}

variable "roles" {
  description = "Defines list of roles which will be granted to this new role."
  type        = list(string)
  default     = null
}

variable "search_path" {
  description = "Alters the search path of this new role. Note that due to limitations in the implementation, values cannot contain the substring ', '."
  type        = list(string)
  default     = null
}

variable "valid_until" {
  description = "Defines the date and time after which the role's password is no longer valid."
  type        = string
  default     = null
}

variable "skip_drop_role" {
  description = "When a PostgreSQL ROLE exists in multiple databases and the ROLE is dropped, the cleanup of ownership of objects in each of the respective databases must occur before the ROLE can be dropped from the catalog. Set this option to true when there are multiple databases in a PostgreSQL cluster using the same PostgreSQL ROLE for object ownership. This is the third and final step taken when removing a ROLE from a database."
  type        = bool
  default     = true
}

variable "skip_reassign_owned" {
  description = "When a PostgreSQL ROLE exists in multiple databases and the ROLE is dropped, a REASSIGN OWNED in must be executed on each of the respective databases before the DROP ROLE can be executed to dropped the ROLE from the catalog. This is the first and second steps taken when removing a ROLE from a database (the second step being an implicit DROP OWNED)."
  type        = bool
  default     = null
}

variable "statement_timeout" {
  description = "Defines the date and time after which the role's password is no longer valid."
  type        = number
  default     = null
}

variable "database" {
  description = "The database to grant privileges on for this role."
  type        = string
}

variable "privileges" {
  description = "The list of privileges to grant. There are different kinds of privileges: SELECT, INSERT, UPDATE, DELETE, TRUNCATE, REFERENCES, TRIGGER, CREATE, CONNECT, TEMPORARY, EXECUTE, and USAGE. An empty list could be provided to revoke all privileges for this role."
  type        = list(string)
  default     = []
}