resource "postgresql_role" "role" {
  name                      = var.name
  login                     = var.login
  password                  = var.password
  superuser                 = var.superuser
  create_database           = var.create_database
  create_role               = var.create_role
  inherit                   = var.inherit
  replication               = var.replication
  bypass_row_level_security = var.bypass_row_level_security
  connection_limit          = var.connection_limit
  encrypted_password        = var.encrypted_password
  roles                     = var.roles
  search_path               = var.search_path
  valid_until               = var.valid_until
  skip_drop_role            = var.skip_drop_role
  skip_reassign_owned       = var.skip_reassign_owned
  statement_timeout         = var.statement_timeout
}

resource "postgresql_grant" "grant" {
  database    = var.database
  role        = postgresql_role.role.name
  object_type = "database"
  privileges  = var.privileges
}