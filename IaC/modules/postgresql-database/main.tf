resource "postgresql_database" "database" {
  name              = var.name
  owner             = var.owner
  template          = var.template
  lc_collate        = var.lc_collate
  lc_ctype          = var.lc_ctype
  connection_limit  = var.connection_limit
  allow_connections = var.allow_connections
  tablespace_name   = var.tablespace_name
  is_template       = var.is_template
  encoding          = var.encoding
}