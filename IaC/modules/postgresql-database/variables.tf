variable "name" {
  description = "The name of the database. Must be unique on the PostgreSQL server instance where it is configured."
  type        = string
}

variable "owner" {
  description = "The role name of the user who will own the database, or DEFAULT to use the default (namely, the user executing the command). To create a database owned by another role or to change the owner of an existing database, you must be a direct or indirect member of the specified role, or the username in the provider is a superuser."
  type        = string
  default     = null
}

variable "template" {
  description = "The name of the template database from which to create the database, or DEFAULT to use the default template (template0)."
  type        = string
  default     = null
}

variable "lc_collate" {
  description = "Collation order (LC_COLLATE) to use in the database. This affects the sort order applied to strings, e.g. in queries with ORDER BY, as well as the order used in indexes on text columns. If unset or set to an empty string the default collation is set to C."
  type        = string
  default     = "C"
}

variable "lc_ctype" {
  description = "Character classification (LC_CTYPE) to use in the database. This affects the categorization of characters, e.g. lower, upper and digit. If unset or set to an empty string the default character classification is set to C."
  type        = string
  default     = "C"
}

variable "connection_limit" {
  description = "How many concurrent connections can be established to this database. -1 (the default) means no limit."
  type        = number
  default     = -1
}

variable "allow_connections" {
  description = "If false then no one can connect to this database. The default is true, allowing connections (except as restricted by other mechanisms, such as GRANT or REVOKE CONNECT)."
  type        = bool
  default     = true
}

variable "tablespace_name" {
  description = "The name of the tablespace that will be associated with the database, or DEFAULT to use the template database's tablespace. This tablespace will be the default tablespace used for objects created in this database."
  type        = string
  default     = null
}

variable "is_template" {
  description = "If true, then this database can be cloned by any user with CREATEDB privileges; if false (the default), then only superusers or the owner of the database can clone it."
  type        = bool
  default     = false
}

variable "encoding" {
  description = "Character set encoding to use in the database. Specify a string constant (e.g. UTF8 or SQL_ASCII), or an integer encoding number. If unset or set to an empty string the default encoding is set to UTF8."
  type        = string
  default     = "UTF8"
}