resource "aws_db_instance" "db_instance" {
  allocated_storage      = var.allocated_storage
  max_allocated_storage  = var.max_allocated_storage
  storage_type           = var.storage_type
  engine                 = var.engine
  engine_version         = var.engine_version
  instance_class         = var.instance_class
  db_name                = var.db_name
  username               = var.username
  password               = var.password
  db_subnet_group_name   = var.db_subnet_group_name
  identifier_prefix      = var.identifier_prefix
  monitoring_interval    = var.monitoring_interval
  monitoring_role_arn    = var.monitoring_role_arn
  multi_az               = var.multi_az
  port                   = var.port
  publicly_accessible    = var.publicly_accessible
  vpc_security_group_ids = concat([module.rds_security_group.id], var.vpc_security_group_ids)

  option_group_name           = var.option_group_name
  parameter_group_name        = var.parameter_group_name
  skip_final_snapshot         = var.skip_final_snapshot
  allow_major_version_upgrade = var.allow_major_version_upgrade
  apply_immediately           = var.apply_immediately
  auto_minor_version_upgrade  = var.auto_minor_version_upgrade
  maintenance_window          = var.maintenance_window

  availability_zone = var.availability_zone

  backup_retention_period   = var.backup_retention_period
  backup_window             = var.backup_window
  delete_automated_backups  = var.delete_automated_backups
  final_snapshot_identifier = var.final_snapshot_identifier

  copy_tags_to_snapshot                 = var.copy_tags_to_snapshot
  deletion_protection                   = var.deletion_protection
  enabled_cloudwatch_logs_exports       = var.enabled_cloudwatch_logs_exports
  performance_insights_enabled          = var.performance_insights_enabled
  performance_insights_kms_key_id       = var.performance_insights_kms_key_id
  performance_insights_retention_period = var.performance_insights_retention_period

  tags = merge({ "Environment" = upper(var.environment), "Service" = var.service, "ManagedByTerraform" = "True" }, var.tags)
}

# Backup
data "aws_iam_policy_document" "aws-backup-service-assume-role-policy" {
  statement {
    sid     = "AssumeServiceRole"
    actions = ["sts:AssumeRole"]
    effect  = "Allow"
    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }
  }
}

data "aws_iam_policy_document" "assume_role" {
  statement {
    effect = "Allow"

    principals {
      type        = "Service"
      identifiers = ["backup.amazonaws.com"]
    }

    actions = ["sts:AssumeRole"]
  }
}
resource "aws_iam_role" "backup_iam_role" {
  count = var.backup ? 1 : 0

  name               = join("-", [var.service, var.environment])
  assume_role_policy = data.aws_iam_policy_document.assume_role.json
}

resource "aws_iam_role_policy_attachment" "backup_policy_attachment" {
  count = var.backup ? 1 : 0

  policy_arn = "arn:aws:iam::aws:policy/service-role/AWSBackupServiceRolePolicyForBackup"
  role       = aws_iam_role.backup_iam_role[0].name
}

resource "aws_backup_plan" "rds" {
  count = var.backup ? 1 : 0

  name = join("-", [var.service, var.environment])

  rule {
    rule_name         = join("-", [var.service, var.environment])
    target_vault_name = aws_backup_vault.backup_vault[0].name
    schedule          = "cron(0 12 * * ? *)"

    lifecycle {
      delete_after = 7
    }
  }

  tags = merge({ "Environment" = upper(var.environment), "Service" = var.service, "ManagedByTerraform" = "True" }, var.tags)
}

resource "aws_backup_vault" "backup_vault" {
  count = var.backup ? 1 : 0

  name = join("-", [var.service, var.environment])

  force_destroy = var.backup_force_destroy

  tags = merge({ "Environment" = upper(var.environment), "Service" = var.service, "ManagedByTerraform" = "True" }, var.tags)
}

resource "aws_backup_selection" "backup-selection" {
  count = var.backup ? 1 : 0

  iam_role_arn = aws_iam_role.backup_iam_role[0].arn
  name         = "backup_resources"
  plan_id      = aws_backup_plan.rds[0].id
  resources = [
    aws_db_instance.db_instance.arn,
  ]
}

module "rds_security_group" {
  source = "../aws_security_group"

  description = join(" ", [upper(var.environment), "RDS Security Group"])
  egress_rules = [{
    description = "Allow internet access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  environment = upper(var.environment)
  ingress_rules = [{
    description     = "App to DB access"
    from_port       = var.port
    to_port         = var.port
    protocol        = "TCP"
    cidr_blocks     = []
    security_groups = var.vpc_security_group_ids
    self            = true
    }, {
    description     = "Management DB access"
    from_port       = var.port
    to_port         = var.port
    protocol        = "TCP"
    cidr_blocks     = ["10.234.18.219/32"]
    security_groups = ["493643765515/sg-06eab749ec333597f"]
    }
  ]
  name    = join("-", [var.environment, var.service, "rds-sg"])
  service = var.service
  vpc_id  = var.vpc_id
}
