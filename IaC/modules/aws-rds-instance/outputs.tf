output "id" {
  value = aws_db_instance.db_instance.id
}

output "address" {
  value = aws_db_instance.db_instance.address
}

output "port" {
  value = aws_db_instance.db_instance.port
}

output "db_name" {
  value = aws_db_instance.db_instance.db_name
}

output "username" {
  value = aws_db_instance.db_instance.username
}

output "engine" {
  value = aws_db_instance.db_instance.engine
}

output "engine_version_actual" {
  value = aws_db_instance.db_instance.engine_version_actual
}

output "sg_id" {
  value = module.rds_security_group.id
}