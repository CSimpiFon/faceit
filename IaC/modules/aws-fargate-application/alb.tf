module "alb_security_group" {
  source = "../aws_security_group"

  description = join(" ", [upper(var.environment), "ALB Security Group"])
  egress_rules = [{
    description = "Allow inbound traffic to ALB"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  environment   = var.environment
  ingress_rules = []
  name          = join("-", [var.service, "alb", var.environment])
  service       = var.service
  vpc_id        = var.vpc_id
}

resource "aws_security_group_rule" "http" {
  from_port         = 80
  protocol          = "tcp"
  security_group_id = module.alb_to_fargate_security_group.id
  to_port           = 80
  type              = "ingress"
  cidr_blocks       = ["0.0.0.0/0"]
}

module "alb_to_fargate_security_group" {
  source = "../aws_security_group"

  description = join(" ", [upper(var.environment), "ALB to Fargate Security Group"])
  egress_rules = [{
    description = "LB to Fargate access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
    }
  ]
  environment = var.environment
  ingress_rules = [{
    description = "LB to Fargate access"
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    self        = true
    }
  ]
  name    = join("-", [var.service, "alb-to-fargate", var.environment])
  service = var.service
  vpc_id  = var.vpc_id
}

module "alb" {
  source = "terraform-aws-modules/alb/aws"

  name               = join("-", [var.service, var.environment])
  load_balancer_type = "application"
  internal           = var.internal_alb
  vpc_id             = var.vpc_id
  subnets            = var.public_subnet
  security_groups    = [module.alb_to_fargate_security_group.id, module.alb_security_group.id]
  idle_timeout       = 600
  http_tcp_listeners = [
    {
      port        = 80
      protocol    = "HTTP"
      priority    = 100
      action_type = "fixed-response"
      fixed_response = {
        content_type = "text/plain"
        status_code  = "503"
      }
    }
  ]

  target_groups = [
    {
      name             = join("-", [var.service, "lb", var.environment])
      backend_protocol = "HTTP"
      protocol_version = var.backend_protocol_version
      target_type      = "ip"
      backend_port     = 8080
      health_check = {
        path                = "/health"
        matcher             = "200"
        timeout             = 5
        healthy_threshold   = 5
        unhealthy_threshold = 2
      }
    }
  ]
  tags = {
    Service     = var.service
    Environment = var.environment
  }
}

resource "aws_alb_listener_rule" "https" {
  listener_arn = module.alb.http_tcp_listener_arns[0]
  priority     = 100
  action {
    type             = "forward"
    target_group_arn = module.alb.target_group_arns[0]
  }
  condition {
    source_ip {
      values = ["0.0.0.0/0"]
    }
  }

  tags = {
    Service     = var.service
    Environment = var.environment
  }
}