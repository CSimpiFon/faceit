variable "vpc_id" {
  description = "AWS VPC ID"
  type        = string
}

variable "internal_alb" {
  default     = false
  description = "type of ALB to use"
}

variable "public_subnet" {
  description = "list of subnets to use for ALB"
  type        = list(string)
}

variable "backend_protocol_version" {
  description = "Can be either HTTP1 or HTTP2 if supported"
  type        = string
  default     = "HTTP1"
}

variable "docker_image" {
  description = "Docker image to use"
  type        = string
}

variable "postgres_host" {
  description = "Service name"
  type        = string
}

variable "postgres_port" {
  description = "Service name"
  type        = string
}

variable "postgres_user" {
  description = "Service name"
  type        = string
}

variable "postgres_password" {
  description = "Service name"
  type        = string
}

variable "postgres_db" {
  description = "Service name"
  type        = string
}

variable "service" {
  description = "Service name"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
}

variable "containerinsights" {
  type    = string
  default = "disabled"
}

variable "desired_count" {
  type    = number
  default = 1
}

variable "security_groups" {
  description = "List of Security Groups attached to the ECS service"
  type        = list(string)
}

variable "private_subnet" {
  description = "List of private subnet to give IP to the ECS service in"
  type        = list(string)
}

variable "container_name" {
  description = "Name of the container to spin up within the ECS"
  type        = string
}

variable "port" {
  description = "Port number the container and application is listening on"
  type        = number
  default     = 8080
}

variable "task_cpu" {
  description = "The number of cpu units used by the task"
  type        = number
  default     = 256
}

variable "task_memory" {
  description = "The amount (in MiB) of memory used by the task"
  type        = number
  default     = 512
}

variable "min_task" {
  description = "Minimum number of tasks should the service scale to"
  type        = string
  default     = "1"
}

variable "max_task" {
  description = "Maximum number of tasks should the service scale to"
  type        = string
  default     = "2"
}

variable "scaling_up_cooldown" {
  description = "The amount of time, in seconds, after a scaling activity completes and before the next scaling activity can start (upscaling)"
  type        = string
  default     = "60"
}

variable "scaling_up_adjustment" {
  description = "Maximum number of tasks should the service scale to"
  type        = string
  default     = "1"
}

variable "scaling_down_cooldown" {
  description = "Maximum number of tasks should the service scale to"
  type        = string
  default     = "300"
}

variable "scaling_down_adjustment" {
  description = "Maximum number of tasks should the service scale to"
  type        = string
  default     = "-1"
}