output "alb_to_fargate_sg_id" {
  value = module.alb_to_fargate_security_group.id
}

output "alb_address" {
  value = module.alb.lb_dns_name
}