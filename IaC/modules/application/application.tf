module "application" {
  source = "../aws-fargate-application"

  container_name    = var.service
  environment       = var.environment
  postgres_db       = join("-", [var.environment, var.service])
  postgres_host     = module.rds.address
  postgres_password = var.db_app_password
  postgres_port     = module.rds.port
  postgres_user     = join("-", [var.environment, var.service])
  private_subnet    = var.private_subnets
  public_subnet     = var.public_subnets
  security_groups   = [module.rds.sg_id]
  service           = var.service
  vpc_id            = var.vpc_id
  docker_image      = var.docker_image
  desired_count     = var.desired_count
  min_task          = var.min_task
  max_task          = var.max_task
  task_cpu          = var.task_cpu
  task_memory       = var.task_memory

  depends_on = [module.rds]
}