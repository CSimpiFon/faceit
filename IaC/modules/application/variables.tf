variable "service" {
  description = "Service name"
  type        = string
}

variable "environment" {
  description = "Environment name"
  type        = string
}

variable "aws_region" {
  description = "AWS Region"
  type        = string
  default     = "eu-west-1"
}

variable "vpc_id" {
  description = "AWS VPC ID"
  type        = string
}

variable "db_instance_class" {
  description = "Type of the DB EC2 instance"
  type        = string
  default     = "db.t4g.micro"
}

variable "db_multi_az" {
  description = "If the RDS is multi-az"
  type        = bool
  default     = false
}

variable "security_group_ids" {
  description = "Additional Security Group IDs"
  type        = list(string)
  default     = []
}

variable "backup" {
  description = "Enable/Disable backup"
  type        = bool
  default     = false
}

variable "db_root_username" {
  description = "Username for root DB user"
  type        = string
  default     = "root"
}

variable "db_root_password" {
  description = "Length of DB root password"
  type        = string
}

variable "db_app_password" {
  description = "Length of DB app password"
  type        = string
}

variable "private_subnets" {
  description = "Private subnets to put the app in"
  type        = list(string)
}

variable "public_subnets" {
  description = "Public subnets to put the LB in"
  type        = list(string)
}

variable "docker_image" {
  description = "Container of the application"
  type        = string
}

variable "desired_count" {
  description = "Desired count of the application replicas"
  type        = number
  default     = 1
}

variable "min_task" {
  description = "Minimum amount of replicas to run"
  type        = number
  default     = 1
}

variable "max_task" {
  description = "Maximum amount of replicas to run"
  type        = number
  default     = 2
}

variable "task_cpu" {
  description = "Minimum amount of replicas to run"
  type        = number
  default     = 256
}

variable "task_memory" {
  description = "Maximum amount of replicas to run"
  type        = number
  default     = 512
}