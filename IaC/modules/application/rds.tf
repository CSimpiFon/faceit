module "rds" {
  source = "../aws-rds-instance"

  db_subnet_group_name   = join("-", [var.environment, "db", var.aws_region])
  engine                 = "postgres"
  identifier_prefix      = join("-", [var.environment, var.service])
  instance_class         = var.db_instance_class
  multi_az               = var.db_multi_az
  password               = var.db_root_password
  environment            = upper(var.environment)
  service                = var.service
  username               = var.db_root_username
  vpc_security_group_ids = var.security_group_ids
  db_name                = var.service
  backup                 = var.backup
  vpc_id                 = var.vpc_id
}

module "database" {
  source = "../postgresql-database"

  name = join("-", [var.environment, var.service])

  depends_on = [module.rds]
}

module "role" {
  source = "../postgresql-role"

  name     = join("-", [var.environment, var.service])
  password = var.db_app_password
  login    = true

  database   = module.rds.db_name
  privileges = ["CONNECT", "CREATE", "TEMPORARY"]

  depends_on = [module.database]
}