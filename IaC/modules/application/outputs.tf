output "lb_url" {
  value = module.application.alb_address
}

output "pg_url" {
  value = module.rds.address
}

output "pg_port" {
  value = module.rds.port
}

output "pg_root_user" {
  value = module.rds.username
}