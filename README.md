<h2>FACEIT DevOps Challenge</h2>

<h3>Architecture Diagram</h3>
![Architecture Diagram](./Architecture.png "Architecture Diagram")

<h3>Tools Used</h3>

- JUnit - Code Quality Tests
- Trivy - Security checking (Code and container)
- Golangci-lint - GoLang Linters
- Kaniko - Container build
- Terraform - Infrastructure as Code
- GitLab CI - CI pipeline run

<h3>Code Structure</h3>

- Application code and Docker file is stored in the <b>test-app</b> directory
- Infrastructure as Code terraform files are stored in the <b>IaC</b> directory
- The Terraform modules stored in the <b>IaC/modules</b> directory
- CI/CD pipeline is written in the <b>.gitlab-ci.yml</b>

<h3>Initialization</h3>

In order to be able to initialize the application with the Terraform code, you will need the followings:

- An AWS account with proper permissions
- AWS VPCs with Name tags <b>uat</b> and <b>prd</b>
- Database subnet group with name <b>{environment}-db-{aws_region}</b> (Example: uat-db-eu-west-1)
- At least 2 private subnets for the containers to run in, with name <b>{environment}-backend-{aws_region}a/b/c</b> (Example: uat-backend-eu-west-1a, uat-backend-eu-west-1b)
- At least 2 public subnets for the ELB to get IP from, with name <b>{environment}-public-{aws_region}a/b/c</b> (Example: uat-public-eu-west-1a, uat-public-eu-west-1b)
- A container to spin up in Fargate
- If IaC is not run by the automation in this project, the <b>APP_VERSION</b> and <b>environment</b> variables should be set in vars.tf or by <b>TF_VAR_APP_VERSION</b> and <b>TF_VAR_environment</b> environmental variables
- The deployed service's access URL is printed out by Terraform as <b>lb_url</b> and it can be reached on <b>http://{lb_url}/health</b>. The response of the working application is <b>UP</b>

<h3>Deployment Management</h3>

The deployment can be changed in the IaC code, by changing variables in the <b>IaC/{environment}/vars.tf</b> and <b>application.tf</b> files

- To change the name of the deployment, change the service name by the <b>service</b> variable in vars.tf
- To change the environment of the deployment, change the environment name by the <b>environment</b> variable in vars.tf
- To enable the backup of the RDS instance, set the <b>backup</b> variable to true in the application.tf
- To destroy the deployment run the pipeline with the <b>{cleanup: true}</b> variable

<h3>Scaling</h3>

- The scaling of the application is automated by relying on the autoscaling provided by AWS Fargate
- Can be scaled horizontally manually by changing the <b>desired_count</b>, <b>min_task</b> and <b>max_task</b> variables in the application.tf
- Can be scaled vertically manually by changing the <b>task_cpu</b> and <b>task_memory</b> variables in the application.tf
- The scaling of the database can be done by changing the <b>db_instance_class</b> variable in the application.tf

<h3>Automation</h3>

Automation is written to run Go linter and security check on the GoLang application stored in the <b>test-app</b> directory, build a small footprint container from it, and deploy into AWS fargate with all the necessary infrastructure required by it to run.<br>
The pipeline only runs if a tag is added to a commit. The tag must follow the regular expression ^v\d+\.\d+\.\d+$<br>
Fargate deploys the new version of the application without downtime, first bringing up the new version, then stopping the previous one.<br>
The pipeline deploys 2 environments called uat and prd.

<h3>Monitoring</h3>

Monitoring can be done through CloudWatch, and/or a provider-agnostic way, a Prometheus server can be deployed with Grafana to collect metrics and visualize them.

<h3>Security</h3>

<h4>Some security features are already implemented</h4>

- Complex database credentials are generated on deployment, so no need to store them in git
- Security checks are run against the codebase and container image too
- Communication to the database has been restricted to the application and Gitlab runner
- Application is deployed into a backend subnet, and only reachable through the load balancer
- Terraform state is stored in a secured shared state store encrypted in transit and on the storage too
- Terraform states are separated per environment to lower blast radius
- Possibility to create database backups is implemented

<h4>Security features remain to apply</h4>

- The application currently listen on unencrypted HTTP protocol, this should be moved to HTTPS with a trusted certificate
- Store the application credentials in a safe secret store, like HashiCorp Vault, and populate the environmental variables from there
- Change the application to read the configuration from secret store and not from environmental variables

<h3>Features to be added</h3>

- CI to be amended to be able to manage multiple environment (Child pipelines)
- Add unit tests to check application functionalities before deployment
- Add E2E tests to test if the application works as expected before deployment
- Add automated QA tests to ensure the application behaves as expected after deployment
- Move all the Terraform modules into a centralized module repository, so they can be reused in more projects, and easier to maintain them by multiple engineers
- Logs of the application to be sent into a centralized logging system like ElasticSearch and Kibana
- The application can be put behind CDN like CloudFront to service the clients from cache and closer to the clients to lower response times